-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : jeu. 30 avr. 2020 à 15:32
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tes`
--

-- --------------------------------------------------------

--
-- Structure de la table `TE`
--

CREATE TABLE `TE` (
  `id` int(120) NOT NULL,
  `nom` varchar(70) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `email` text NOT NULL,
  `Adresse1` varchar(200) DEFAULT NULL,
  `Adresse2` varchar(200) DEFAULT NULL,
  `VILLE` varchar(200) DEFAULT NULL,
  `Numero_de_telephone` int(20) DEFAULT NULL,
  `Code_postal` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `TE`
--

INSERT INTO `TE` (`id`, `nom`, `prenom`, `email`, `Adresse1`, `Adresse2`, `VILLE`, `Numero_de_telephone`, `Code_postal`) VALUES
(1, '', '', '', NULL, NULL, NULL, NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `TE`
--
ALTER TABLE `TE`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `TE`
--
ALTER TABLE `TE`
  MODIFY `id` int(120) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
